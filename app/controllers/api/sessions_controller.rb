module Api
  class SessionsController < ApplicationController
    skip_before_action :authenticate_user_from_token!

    # POST /api/login curl localhost:3000/api/login --ipv4 --data "email=user@example.com&password=password"
    def create
      if !params[:facebook_user_id].nil? # Getting just user ID for Facebook login
        @user = User.where(facebook_id: params[:facebook_user_id]).take
        render json: { userId: @user.id }
        return
      end

      @user = User.find_for_database_authentication(email: params[:email])
      return invalid_login_attempt unless @user

      if @user.valid_password?(params[:password])
        sign_in :user, @user
        render json: @user, serializer: SessionSerializer, root: nil
      else
        invalid_login_attempt
      end
    end

    def register
      user = User.new
      user.email = params[:email]
      user.password = params[:password]
      user.facebook_id = params[:facebook_user_id]

      if user.save
        render :json => user.as_json(
          :access_token => user.access_token,
          :email => user.email), :status => 201
        return
      else
        warden.custom_failure!
        render :json => user.errors, :status => 422
      end
    end

    private

    def invalid_login_attempt
      warden.custom_failure!
      render json: { error: t('sessions_controller.invalid_login_attempt') }, status: :unprocessable_entity
    end
  end
end
